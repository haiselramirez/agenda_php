<!-- Modal -->
<div class="modal fade" id="mAgregarContacto" tabindex="-1" aria-labelledby="mAgregarContacto" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-info text-white">
        <h5 class="modal-title">Agregar una Contacto</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="" method="POST" id="frmAgregarContacto">
        <div class="modal-body">
          <div class="form-group mb-3">
            <label for="tNombre">Nombre</label>
            <input type="text" id="tNombre" name="tNombre" class="form-control" placeholder="Nombre de Contacto">
          </div>
          <div class="form-group mb-3">
            <label for="tDescripcion">Descripción</label>
            <textarea id="tDescripcion" name="tDescripcion" class="form-control" placeholder="Descripción de Contacto"></textarea>
          </div>
        </form>
      </div>
        <div class="modal-footer">
          <button type="reset" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-info">Guardar</button>
        </div>
      </form>
    </div>
  </div>
</div>