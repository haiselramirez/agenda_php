<div class="card">
  <div class="card-body table-responsive">
    <table class="table table-hover table-bordered table-sm">
      <thead class="bg-info text-center text-white">
        <tr>
          <th>Nombre</th>
          <th>Descripcion</th>
          <th><i class="fas fa-edit"></i> Editar</th>
          <th><i class="fas fa-trash"></i> Eliminar</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td>
            <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#mActualizarCategoria">
              <i class="fas fa-edit"></i>
            </button>
          </td>
          <td>
            <button class="btn btn-sm btn-danger" onclick="eliminarCategoria();">
              <i class="fas fa-trash"></i>
            </button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>