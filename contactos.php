<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="public/img/favicon.ico" type="image/x-icon">
  <title>Agenda | Contactos</title>
  <?php include_once "dependencias.php";?>
</head>
<body class="bg-secondary">
  <div class="container mt-5">
  <?php include_once 'menu.php';?>
  <div class="jumbotron jumbotron-fluid p-3">
    <h1 class="display-4"><i class="fas fa-users mr-3"></i> Contactos</h1>
    <button class="btn btn-info" data-toggle="modal" data-target="#mAgregarContacto">
      <i class="fas fa-user-plus"></i>
      <strong>Nuevo Contanto</strong>
    </button>
    <hr class="my-4">
    <div id="cargaTablaContacto">

    </div>
    <?php
      require_once "vistas/contactos/modalAgregar.php";
      require_once "vistas/contactos/modalActualizar.php";
    ?>
  </div>

  <script src="public/js/contacto.js"></script>
</body>
</html>