<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="public/img/favicon.ico" type="image/x-icon">
  <title>Agenda | Categorias</title>
  <?php include_once "dependencias.php";?>
</head>
<body class="bg-secondary">
  <div class="container mt-5">
  <?php include_once 'menu.php';?>

  <div class="jumbotron jumbotron-fluid p-3">
    <h1 class="display-4"><i class="fas fa-tags mr-3"></i> Categorias</h1>
    <button class="btn btn-info" data-toggle="modal" data-target="#mAgregarCategoria">
      <i class="fas fa-plus"></i>
      <strong>Nueva Categoría</strong>
    </button>
    <hr class="my-4">
    <div id="getTblcategoria"></div>
    <?php
      require_once "vistas/categorias/modalAgregar.php";
      require_once "vistas/categorias/modalActualizar.php";
    ?>
  </div>


  <script src="public/js/categorias.js"></script>
</body>
</html>