$(function(){
  $("#getTblcategoria").load("vistas/categorias/tablaCategorias.php");
  $("#getTblCategoria").dataTable();
});


function eliminarCategoria(){
  swal({
    title: "¿Está seguro de eliminar esta categoría?",
    text: "Una vez eliminado no se podra recuperar!",
    icon: "warning",
    buttons: true,
    dangerMode: true,
  })
  .then((willDelete) => {
    if (willDelete) {
      swal("Se ha eliminado!");
    }
  })
}