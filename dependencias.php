<!-- css -->
<link rel="stylesheet" href="public/css/bootstrap.min.css">
<link rel="stylesheet" href="public/css/all.min.css">
<link rel="stylesheet" href="public/css/DataTables.bootstrap.css">
<link rel="stylesheet" href="public/css/dataTables.bootstrap4.min.css">

<!-- js -->
<script src="public/js/jquery-3.6.0.min.js"></script>
<script src="public/js/popper.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/bootstrap.bundle.min.js"></script>
<script src="public/js/all.min.js"></script>
<script src="public/js/sweetalert.min.js"></script>