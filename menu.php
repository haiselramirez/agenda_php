<nav class="navbar navbar-expand-lg navbar-dark bg-info">
  <div class="container">
    <a class="navbar-brand" href="index.php" >
      <img src="public/img/logo.png" width="100" height="45" alt="">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu1" aria-controls="menu1" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="menu1">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <i class="fas fa-home"></i>
            <strong class="text-white"> Inicio</strong>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="contactos.php">
            <i class="fas fa-users"></i>
            <strong class="text-white"> Contactos</strong>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="categorias.php">
            <i class="fas fa-tags"></i>
            <strong class="text-white"> Categorias</strong>
          </a>
        </li>
      </ul>
    </div>
  </div>
</nav>