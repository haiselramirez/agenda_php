<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" href="public/img/favicon.ico" type="image/x-icon">
  <title>Agenda | inicio</title>
  <?php include_once "dependencias.php";?>
</head>
<body class="">
  <div class="container mt-0">
  <?php include_once 'menu.php';?>
  <!-- Header -->
  <div class="jumbotron jumbotron-fluid bg-info py-0 mb-0">
    <div class="container text-center">
      <img src="public/img/logo.png" alt="" class="img-fluid" width="280">
      <h5 class="text-white mt-0 pb-2">Agenda de contactos con PHP y MySQL</h5>
    </div>
  </div>
  <!-- contenido -->
  <div class="row">
    <div class="col-sm-5">
      <div class="card border-none">
        <div class="card-body">
          <img src="public/img/foto.jpg" alt="" class="img-fluid" width="400">
        </div>
      </div>
    </div>
    <div class="col-sm-7">
      <ul class="list-group mb-3">
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Favoritos</h6>
            <small class="text-muted">Brief description</small>
          </div>
          <span class="text-muted">8</span>
        </li>
        <li class="list-group-item d-flex justify-content-between lh-condensed">
          <div>
            <h6 class="my-0">Familia</h6>
            <small class="text-muted">Famila materna y paterna</small>
          </div>
          <span class="text-muted">15</span>
        </li>
        <li class="list-group-item d-flex justify-content-between">
          <div class="">
            <h6 class="my-0">Compañeros</h6>
            <small>Mis compañeros de trabajo y de la Universidad</small>
          </div>
          <span class="text-success">10</span>
        </li>
        <li class="list-group-item d-flex justify-content-between">
          <span>Total</span>
          <strong>33</strong>
        </li>
      </ul>
    </div>
  </div>
</body>
</html>